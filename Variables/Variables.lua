SMARTM_Variables = {}
SMARTM_Variables.__index = SMARTM_Variables

--noinspection LuaOverlyLongMethod
function SMARTM_Variables:Create()
	local Variables = {}
	setmetatable(Variables, SMARTM_Variables)
	
	Variables.DEBUG_NONE = 1
	
	--no mutes
	--no whispers
	--no announces
	Variables.DEBUG_MUTE = 2
	
	--all from up and:
	--test channels
	Variables.DEBUG_CHANNEL = 3
	
	--all form up and:
	--no history requests
	Variables.DEBUG_FULL = 4
	
	Variables.DebugMode = Variables.DEBUG_NONE
	if Variables.DebugMode < Variables.DEBUG_CHANNEL then
		Variables.CHANNEL_LFG = 'ПОИСК СПУТНИКОВ'
		Variables.CHANNEL_WTC = 'ТОРГОВЛЯ: ГОРОД'
	else
		Variables.CHANNEL_LFG = 'ТЕСТОВЫЙ_ЛФГ'
		Variables.CHANNEL_WTC = 'ТЕСТОВЫЙ_ВТЦ'
	end
	
	
	
	--command variables
	Variables.CMD_HISTORY = '.mutelist char full %s'
	Variables.CMD_MUTE = '.mute %s %s %s'
	
	--output formats
	Variables.FORMAT_DATETIME = '%d.%m.%Y %H:%M:%S'
	Variables.FORMAT_TIME = '%H:%M:%S'
	Variables.FORMAT_CHAT = '[%s] %s: %s'
	
	--pattern variables
	Variables.PATTERN_HISTORY_DELIMITER = '=============================='
	Variables.PATTERN_HISTORY_CHARACTER = 'История мутов персонажа (.+):'
	Variables.PATTERN_HISTORY_DATETIME = '(%d+)-(%d+)-(%d+)_(%d+)-(%d+)-(%d+)'
	Variables.PATTERN_HISTORY_TARGET = '|cffffffff|Hplayer:.+|h%[(.+)%]|h|r'
	Variables.PATTERN_HISTORY_NEWNAME = ': (.+), текущий ник: |cffffffff|Hplayer:.+|h%[(.+)%]|h|r'
	Variables.PATTERN_HISTORY_DELETED = '(%d+)-(%d+)-(%d+)_(%d+)-(%d+)-(%d+): (.+), удален'
	Variables.PATTERN_HISTORY_MUTEACCOUNT = 'текущий аккаунт: (.+)'
	Variables.PATTERN_HISTORY_ACCOUNT = 'История мутов аккаунта (.+):'
	Variables.PATTERN_HISTORY_MUTETIME = '(%d+) мин, выдал: '
	Variables.PATTERN_HISTORY_AUTHOR = 'выдал: (.+), причина: '
	Variables.PATTERN_HISTORY_REASON = 'причина: (.+)'
	
	--output texts variables
	Variables.TEXT_ANNOUNCE = 'Чат [%s] заблокирован на %s.'
	Variables.TEXT_WARN = 'Чат заблокирован на 1 минуту. Данная блокировка носит предупредительный характер: пожалуйста, соблюдайте правила сервера.'
	Variables.TEXT_FREQUENT_WARN = 'Вы нарушаете данный пункт правил уже %s. Систематическое нарушение может привести к увеличению срока блокировки!'
	Variables.TEXT_FREQUENT_DESC = 'Вы нарушили данный пункт правил %s. В соответствии с правилами сервера, срок блокировки увеличен в %s.'
	Variables.TEXT_GMANNOUNCE = '|TInterface\\CHATFRAME\\UI-ChatIcon-Blizz.png:0:2:0:0|t |cFF00FFFF|Hplayer:%s|h[%s]|h: Чат |Hplayer:%s|h[%s]|h заблокирован на %s: %s|r|cFF00FFFF|r'
	Variables.TEXT_HISTORY = '|cFFCCCCCC%s (%s назад) %s:|r %s "%s" |cFFCCCCCCна %s|r'
	Variables.TEXT_HISTORY_RENAMED = '%s |cFF330000(переименован в %s)|r'
	Variables.TEXT_HISTORY_INTARGET = '|cFFFFFF00%s|r'
	Variables.TEXT_HISTORY_OUTTARGET = '|cFF999900%s|r'
	Variables.TEXT_CHECKBUTTON_STATS = 'Д-%s, Н-%s, М-%s, Г-%s, В-%s'
	
	--addon opening mode
	Variables.MODE_MUTE = 1
	Variables.MODE_WARN = 2
	Variables.MODE_HELP = 3
	Variables.MODE_DIRECT = 4
	
	--addon mute action codes
	Variables.ACTION_QUIET = 1
	Variables.ACTION_MUTE = 2
	Variables.ACTION_WARN = 3
	
	--mute codes
	Variables.MUTE_TRADE = 1
	Variables.MUTE_OFFTOPIC = 2
	Variables.MUTE_OVERPOST = 3
	Variables.MUTE_CAPS = 4
	Variables.MUTE_LEXIS = 5
	Variables.MUTE_HIDDEN = 6
	Variables.MUTE_PROVOKE = 7
	Variables.MUTE_ANGER = 8
	Variables.MUTE_ADMIN = 9
	Variables.MUTE_NOOB = 10
	Variables.MUTE_REAL = 11
	Variables.MUTE_GOLD = 12
	
	--system filter modes
	Variables.SYSFILTER_DIRECT = 1
	Variables.SYSFILTER_HISTORY = 2
	
	--history processing modes
	Variables.HISTORY_NONE = 1
	Variables.HISTORY_CHARACTER = 2
	Variables.HISTORY_ACCOUNT = 3
	Variables.HISTORY_DELETED = 4
	
	--history statistics interval
	Variables.HISTORY_STATS_DAY = 1
	Variables.HISTORY_STATS_WEEK = 2
	Variables.HISTORY_STATS_MONTH = 3
	Variables.HISTORY_STATS_YEAR = 4
	Variables.HISTORY_STATS_ALL = 5
	
	--history statistics modes
	Variables.HISTORY_STATS_TABLE = {
		{Variables.HISTORY_STATS_DAY, 60 * 60 * 24},
		{Variables.HISTORY_STATS_WEEK, 60 * 60 * 24 * 7},
		{Variables.HISTORY_STATS_MONTH, 60 * 60 * 24 * 30},
		{Variables.HISTORY_STATS_YEAR, 60 * 60 * 24 * 365}
	}
	Variables.HISTORY_STATS_TABLE_INTERVAL = 1
	Variables.HISTORY_STATS_TABLE_SECONDS = 2
	
	
	--filtered texts
	Variables.FILTER_SAVED = 'Игрок сохранен.'
	
	return Variables
end