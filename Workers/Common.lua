SMARTM_CommonWorker = {}
SMARTM_CommonWorker.__index = SMARTM_CommonWorker

function SMARTM_CommonWorker:Create(Variables)
	local CommonWorker = {}
	setmetatable(CommonWorker, SMARTM_CommonWorker)
	
	CommonWorker.Variables = Variables
	
	return CommonWorker
end



function SMARTM_CommonWorker:SendCommand(Command)
	if self.Variables.DebugMode < self.Variables.DEBUG_FULL then
		SendChatMessage(Command, 'GUILD')
	else
		print('SendCommand', Command)
	end
end



function SMARTM_CommonWorker:SendWhisper(Target, Message)
	if self.Variables.DebugMode == self.Variables.DEBUG_NONE then
		SendChatMessage(Message, 'WHISPER', nil, Target)
	else
		print('SendWhisper', Target, Message)
	end
end



function SMARTM_CommonWorker:SendAnnounce(Message)
	if self.Variables.DebugMode == self.Variables.DEBUG_NONE then
		SendChatMessage(Message, 'CHANNEL', nil, self:GetLFG())
	else
		print('SendAnnounce', Message)
	end
end



function SMARTM_CommonWorker:GetLFG()
	for ChannelLoop = 1, 10 do
		local _, ChannelName = GetChannelName(ChannelLoop);
		
		if ChannelName and ChannelName:upper() == self.Variables.CHANNEL_LFG then
			return ChannelLoop
		end
	end
end



function SMARTM_CommonWorker:GetServerTime()
	local Hour, Minute = GetGameTime()
	local _, Month, Day, Year = CalendarGetDate()

	return time({year = Year, month = Month, day = Day, hour = Hour, min = Minute, sec = 0})
end



function SMARTM_CommonWorker:CreateForm(Number, Form1, Form2, Form5)
    local Div100 = Number % 100;
    local Div10 = Number % 10;
	
    if (Div100 > 10) and (Div100 < 20) then
		return Form5 
	end
	
    if (Div10 > 1) and (Div10 < 5) then
		return Form2
	end
	
    if Div10 == 1 then
		return Form1
	end
	
    return Form5;
end



function SMARTM_CommonWorker:FormatTime(Value)
	local Hours = 0
	local Minutes = tonumber(Value)

	local Result

	while Minutes >= 60 do
		Minutes = Minutes - 60
		Hours = Hours + 1
	end
	
	if Hours ~= 0 then
		Result = Hours .. self:CreateForm(Hours, ' час', ' часа', ' часов')
	end
	
	if Minutes ~= 0 then
		if Result then
			Result = Result .. ' '
		else
			Result = ''
		end
		Result = Result .. Minutes .. self:CreateForm(Minutes, ' минуту', ' минуты', ' минут')
	end

	return Result	
end



function SMARTM_CommonWorker:FormatTimeDiff(Value)
	Value = math.abs(self:Round(Value / 60))
	if Value < 60 then
		return Value .. self:CreateForm(Value, ' минута', ' минуты', ' минут')
	else
		--hours
		Value = self:Round(Value / 60)
		if Value < 24 then
			return Value .. self:CreateForm(Value, ' час', ' часа', ' часов')
		else
			--days
			Value = self:Round(Value / 24)
			if Value < 7 then
				return Value .. self:CreateForm(Value, ' день', ' дня', ' дней')
			else
				--weaks
				Value = self:Round(Value / 7)
				if Value < 5 then
					return Value .. self:CreateForm(Value, ' неделя', ' недели', ' недель')
				else
					return 'больше месяца'
				end
			end
		end
	end
end



function SMARTM_CommonWorker:Round(Value, Precission)
	local Multiplier = 10^(Precission or 0)
	return math.floor(Value * Multiplier + 0.5) / Multiplier
end



function SMARTM_CommonWorker:Unix2DateTime(Time)
	return date(self.Variables.FORMAT_DATETIME, Time)
end



function SMARTM_CommonWorker:Unix2Time(Time)
	return date(self.Variables.FORMAT_TIME, Time)
end



function SMARTM_CommonWorker:ParseTime(Time)
	local Year, Month, Day, Hour, Minute, Second = Time:match(self.Variables.PATTERN_HISTORY_DATETIME)
	
	if Year and Month and Day and Hour and Minute and Second then
		return time({year = Year, month = Month, day = Day, hour = Hour, min = Minute, sec = Second})
	else
		return nil
	end
end



function SMARTM_CommonWorker:FormatMessage(Message, Channel)
	if not Channel then
		return Message
	end

	return Channel .. '@' .. Message
end



function SMARTM_CommonWorker:CleanupNickname(Nickname)
	local position = string.find(Nickname, '-')

	if position then
		return string.sub(Nickname, 1, position - 1)
	else
		return Nickname
	end
end



function SMARTM_CommonWorker:IsEmpty(Value)
	return Value == nil or Value == ''
end



function SMARTM_CommonWorker:Partition(a,left,right) 
   local pivot = a[right]      
   local i = left               
   for j = left, right-1 do 
      if a[j] <= pivot          
      then 
         a[i],a[j]             
            = a[j],a[i]
         i = i+1
      end 
   end 
   a[i],a[right] = a[right],a[i]
   return i
end 

function SMARTM_CommonWorker:QuickSort(a, left, right)
   if left < right then 
      local q = self:Partition(a,left,right)
      self:QuickSort(a,left,q-1)
      self:QuickSort(a,q+1,right)
   end 
end 
