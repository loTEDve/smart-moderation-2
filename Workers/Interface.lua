SMARTM_InterfaceWorker = {}
SMARTM_InterfaceWorker.__index = SMARTM_InterfaceWorker

function SMARTM_InterfaceWorker:Create(Interface, Variables, Rules, MuteWorker, CommonWorker)
	local InterfaceWorker = {}
	setmetatable(InterfaceWorker, SMARTM_InterfaceWorker)
	
	InterfaceWorker.Interface = Interface
	InterfaceWorker.Variables = Variables
	InterfaceWorker.Rules = Rules
	InterfaceWorker.MuteWorker = MuteWorker
	InterfaceWorker.CommonWorker = CommonWorker
	InterfaceWorker.HistoryHandler = nil
	
	InterfaceWorker.Target = ''
	
	InterfaceWorker.Interface.QuietMuteButton.ButtonFrame:SetScript('OnClick', function()
		_SMARTModeration.InterfaceWorker:OnQuietMute()
	end)
	
	InterfaceWorker.Interface.StandartMuteButton.ButtonFrame:SetScript('OnClick', function()
		_SMARTModeration.InterfaceWorker:OnStandartMute()
	end)
	
	InterfaceWorker.Interface.WarnMuteButton.ButtonFrame:SetScript('OnClick', function()
		_SMARTModeration.InterfaceWorker:OnWarnMute()
	end)
	
	InterfaceWorker.Interface.CloseButton:SetScript('OnClick', function()
		_SMARTModeration.InterfaceWorker:Close()
	end)
	
	InterfaceWorker.ReasonCode = 0
	
	return InterfaceWorker
end



function SMARTM_InterfaceWorker:SetHistoryHandler(HistoryHandler)
	self.HistoryHandler = HistoryHandler
end



function SMARTM_InterfaceWorker:OnQuietMute()
	self.MuteWorker:Action(self.Variables.ACTION_QUIET, self:GetNickname(), self:GetTime(), self:GetReason(), self:GetReasonCode())
	self:Close()
end



function SMARTM_InterfaceWorker:OnStandartMute()
	self.MuteWorker:Action(self.Variables.ACTION_MUTE, self:GetNickname(), self:GetTime(), self:GetReason(), self:GetReasonCode())
	self:Close()
end



function SMARTM_InterfaceWorker:OnWarnMute()
	self.MuteWorker:Action(self.Variables.ACTION_WARN, self:GetNickname(), 1,              self:GetReason(), self:GetReasonCode())
	self:Close()
end



function SMARTM_InterfaceWorker:OpenWrapper()
	_SMARTModeration.InterfaceWorker:Open()
end



function SMARTM_InterfaceWorker:Open(Target, Mode, ReasonCode, Mutliplier)
	Target = self.CommonWorker:CleanupNickname(Target or self.Target)
	Mode = Mode or self.Variables.MODE_DIRECT
	ReasonCode = ReasonCode or self.ReasonCode
	Mutliplier = Mutliplier or 1

	self:SetNickname(Target)
	self.Interface.CheckButtons:ClearCaptions()
	self:ClearMuteHistory()
	
	if (Mode == self.Variables.MODE_MUTE) or (Mode == self.Variables.MODE_WARN) or (Mode == self.Variables.MODE_DIRECT) then
		self:SetReasonCode(ReasonCode)
		
		self:ShowChatHistory(Target)
		--history handler will handle output - nothing needed from worker to display mute history
		self.CommonWorker:SendCommand(self.Variables.CMD_HISTORY:format(Target))
		
		if Mode == self.Variables.MODE_MUTE then
			self.Interface:ActionButtonShadow(self.Interface.WarnMuteButton)
			self.Interface:ActionButtonHighlight(self.Interface.StandartMuteButton)
		end
		
		if Mode == self.Variables.MODE_WARN then
			self.Interface:ActionButtonHighlight(self.Interface.WarnMuteButton)
			self.Interface:ActionButtonShadow(self.Interface.StandartMuteButton)
		end
	end
	
	if Mode == self.MODE_HELP then
		--not implemented
	end
	
	self.Interface.MainFrame:Show()
	self.Interface.ReasonEditBox:SetFocus()
end



function SMARTM_InterfaceWorker:Close()
	self.Interface.MainFrame:Hide()
end



function SMARTM_InterfaceWorker:AddMuteHistory(Data)
	self.Interface.HistoryFrame:Push(Data)
end



function SMARTM_InterfaceWorker:ClearMuteHistory()
	self.Interface.HistoryFrame:Clear()
end



function SMARTM_InterfaceWorker:AddChatHistory(MessageTime, FormatedMessage)
	local Separator = select(2, FormatedMessage:find('@'))

	local Time = self.CommonWorker:Unix2Time(MessageTime)
	
	local Channel = FormatedMessage:sub(1, Separator - 1)
	
	local Message = FormatedMessage:sub(Separator + 1)
	
	self.Interface.ChatFrame:Push(self.Variables.FORMAT_CHAT:format(Time, Channel, Message))
end



function SMARTM_InterfaceWorker:ShowChatHistory(Target)
	self:ClearChatHistory()

	if self.HistoryHandler.ChatHistory[Target] then
		local IndexTable = {}
		local SortTable = {}
		
		for TimeIndex, _ in pairs(self.HistoryHandler.ChatHistory[Target]) do
			for _, MessageValue in pairs(self.HistoryHandler.ChatHistory[Target][TimeIndex]) do
				if not SortTable[TimeIndex] then
					SortTable[TimeIndex] = {}
					table.insert(IndexTable, TimeIndex)
				end
				
				table.insert(SortTable[TimeIndex], MessageValue)		
			end
		end
		
		self.CommonWorker:QuickSort(IndexTable, 1, #IndexTable)
		
		for _, TimeValue in pairs(IndexTable) do
			for _, MessageValue in pairs(SortTable[TimeValue]) do
				self:AddChatHistory(TimeValue, MessageValue)
			end
		end
	end
end



function SMARTM_InterfaceWorker:ClearChatHistory()
	self.Interface.ChatFrame:Clear()
end



function SMARTM_InterfaceWorker:SetReasonCode(ReasonCode)
	self.ReasonCode = ReasonCode
	
	self.Interface:ActionButtonNormal(self.Interface.WarnMuteButton)
	self.Interface:ActionButtonNormal(self.Interface.StandartMuteButton)
	
	if ReasonCode > 0 then
		self.Interface.CheckButtons:Select(ReasonCode)
		self:SetTime(self.Rules:GetTime(ReasonCode))
		self:SetReason(self.Rules:GetShortReason(ReasonCode))
	end
end



function SMARTM_InterfaceWorker:GetReasonCode()
	return self.ReasonCode
end



function SMARTM_InterfaceWorker:SetNickname(Nickname)
	self.Interface.Nickname:SetText(Nickname)
end



function SMARTM_InterfaceWorker:GetNickname()
	return self.Interface.Nickname:GetText()
end



function SMARTM_InterfaceWorker:SetTime(Time)
	self.Interface.TimeEditBox:SetText(Time)
end



function SMARTM_InterfaceWorker:GetTime()
	return self.Interface.TimeEditBox:GetText()
end



function SMARTM_InterfaceWorker:SetReason(Reason)
	self.Interface.ReasonEditBox:SetText(Reason)
end



function SMARTM_InterfaceWorker:GetReason()
	return self.Interface.ReasonEditBox:GetText()
end



function SMARTM_InterfaceWorker:SetTarget(Target)
	self.Target = Target
end



function SMARTM_InterfaceWorker:GetTarget()
	return self.Target
end