SMARTM_ChatFrame = {}
SMARTM_ChatFrame.__index = SMARTM_ChatFrame

--noinspection LuaOverlyLongMethod
function SMARTM_ChatFrame:Create(Interface, ParentFrame)
	local ChatFrame = {}
	setmetatable(ChatFrame, SMARTM_ChatFrame)
	
	ChatFrame.Interface = Interface
	ChatFrame.ParentFrame = ParentFrame
	
	ChatFrame.HolderFrame = CreateFrame('Frame', nil, ChatFrame.ParentFrame)
	ChatFrame.HolderFrame:SetSize(730, 245)
	ChatFrame.HolderFrame:SetFrameStrata('TOOLTIP')	
	ChatFrame.HolderFrame:SetPoint('TOPLEFT', ChatFrame.ParentFrame, 'TOPLEFT', 0, -65)
	ChatFrame.HolderFrame:SetBackdrop({
		bgFile 		= 'Interface\\DialogFrame\\UI-DialogBox-Background',
		edgeFile	= 'Interface\\LFGFrame\\LFGBorder',
		tile 		= true,
		tileSize 	= 32,
		edgeSize 	= 32,
		insets 		= {left = 8, right = 8, top = 8, bottom = 8}
	}) 			  
	ChatFrame.HolderFrame:SetBackdropColor(0, 0, 0, 1)
	ChatFrame.HolderFrame:EnableMouseWheel(true)
	
	ChatFrame.MessageFrame = CreateFrame('ScrollingMessageFrame', nil, ChatFrame.HolderFrame)
	ChatFrame.MessageFrame:SetSize(ChatFrame.HolderFrame:GetWidth() - 50, ChatFrame.HolderFrame:GetHeight() - 30)
	ChatFrame.MessageFrame:SetPoint('TOPLEFT', 15, -10)
	ChatFrame.MessageFrame:SetFont('Fonts\\ARIALN.TTF', 12)
	ChatFrame.MessageFrame:SetTextColor(1, 1, 1, 1)
	ChatFrame.MessageFrame:SetJustifyH('LEFT')
	ChatFrame.MessageFrame:SetHyperlinksEnabled(true)
	ChatFrame.MessageFrame:SetFading(false)
	ChatFrame.MessageFrame:SetMaxLines(1000)
	ChatFrame.MessageFrame:SetIndentedWordWrap(true)

	ChatFrame.ScrollBar = CreateFrame('Slider', nil, ChatFrame.HolderFrame, 'UIPanelScrollBarTemplate')
	ChatFrame.ScrollBar:ClearAllPoints()
	ChatFrame.ScrollBar:SetPoint('RIGHT', ChatFrame.HolderFrame, 'RIGHT', -10, 0)
	ChatFrame.ScrollBar:SetSize(30, ChatFrame.HolderFrame:GetHeight() - 68)
	ChatFrame.ScrollBar:SetMinMaxValues(0, ChatFrame.MessageFrame:GetNumMessages())
	ChatFrame.ScrollBar:SetValueStep(1)	
	ChatFrame.ScrollBar.scrollStep = 1 
	ChatFrame.ScrollBar:SetScript('OnValueChanged', function(this, value)
		local HolderFrame = this:GetParent()
		local FrameObjects = {HolderFrame:GetChildren()}
		
		for _, FrameObject in ipairs(FrameObjects) do
			if FrameObject:GetObjectType() == 'ScrollingMessageFrame' then
				FrameObject:SetScrollOffset(select(2, this:GetMinMaxValues()) - value)
			end
		end
	 end)
	ChatFrame.ScrollBar:SetValue(select(2, ChatFrame.ScrollBar:GetMinMaxValues()))
	
	ChatFrame.HolderFrame:SetScript('OnMouseWheel', function(this, delta)
		local HolderFrame = this;
		local FrameObjects = {HolderFrame:GetChildren()}
		
		local ScrollBar
		local MessageFrame
		for _, FrameObject in ipairs(FrameObjects) do
			if FrameObject:GetObjectType() == 'Slider' then
				ScrollBar = FrameObject
			end
			
			if FrameObject:GetObjectType() == 'ScrollingMessageFrame' then
				MessageFrame = FrameObject
			end
		end
		
		ScrollBar:SetMinMaxValues(MessageFrame:GetNumLinesDisplayed(), MessageFrame:GetNumMessages())
		
		local cur_val = ScrollBar:GetValue()
		local min_val, max_val = ScrollBar:GetMinMaxValues()
		if delta < 0 and cur_val < max_val then
			cur_val = math.min(max_val, cur_val + 1)
			ScrollBar:SetValue(cur_val)			
		elseif delta > 0 and cur_val > min_val then
			cur_val = math.max(min_val, cur_val - 1)
			ScrollBar:SetValue(cur_val)		
		end	
	 end)
	
	return ChatFrame
end



function SMARTM_ChatFrame:Push(Message)
	self.MessageFrame:AddMessage(Message)
	
	self.ScrollBar:SetMinMaxValues(self.MessageFrame:GetNumLinesDisplayed(), self.MessageFrame:GetNumMessages())
	
	self.MessageFrame:ScrollToBottom()
	self.ScrollBar:SetValue(select(2, self.ScrollBar:GetMinMaxValues()))
end



function SMARTM_ChatFrame:Clear()
	self.MessageFrame:Clear()
end