SMARTM_CheckButtons = {}
SMARTM_CheckButtons.__index = SMARTM_CheckButtons

function SMARTM_CheckButtons:Create(Interface, Textures, Rules)
	local CheckButtons = {}
	setmetatable(CheckButtons, SMARTM_CheckButtons)
	
	CheckButtons.Interface = Interface
	CheckButtons.Textures = Textures
	CheckButtons.Rules = Rules
	CheckButtons.Buttons = {}
	CheckButtons.Captions = {}
	
	CheckButtons.HolderFrame = CreateFrame('Frame', nil, Interface.MainFrame)
	CheckButtons.HolderFrame:SetSize(1024, 700)
	CheckButtons.HolderFrame:SetPoint('TOPLEFT')
	CheckButtons.HolderFrame:Show()
	
	return CheckButtons
end



function SMARTM_CheckButtons:DrawInit(OffsetX, OffsetY, WhiteSpace)
	for Index, _ in pairs(self.Rules.MuteList) do
		self:CreateButton(Index, OffsetX, OffsetY, WhiteSpace)
	end
end



function SMARTM_CheckButtons:ClearCaptions()
	for Index = 1, #self.Captions, 1 do
		self:SetCaption(Index, '')
	end
end



function SMARTM_CheckButtons:SetCaption(ReasonCode, Value)
	self.Captions[ReasonCode - 1]:SetText(Value)
end



function SMARTM_CheckButtons:Select(ButtonID)
	local TargetName = 'CheckButton' .. (ButtonID - 1)
	
	local HolderFrame = self.HolderFrame
	local ButtonFrames = {HolderFrame:GetChildren()}
	
	for _, ButtonFrame in ipairs(ButtonFrames) do
		local ButtonObjects = {ButtonFrame:GetChildren()}
		
		for _, ButtonObject in ipairs(ButtonObjects) do
			if ButtonObject:GetObjectType() == 'CheckButton' then
				if ButtonObject:GetName() == TargetName then
					ButtonObject:SetChecked(1)
				else
					ButtonObject:SetChecked(0)
				end
			end
		end
	end
end



function SMARTM_CheckButtons:CreateButton(Index, OffsetX, OffsetY, WhiteSpace)
	local PositionIndex = Index - 1

	if PositionIndex > 0 then
		OffsetY = OffsetY + WhiteSpace
	end

	local ButtonFrame = CreateFrame('Frame', nil, self.HolderFrame)
	ButtonFrame:SetSize(256, 36)
	ButtonFrame:SetPoint('TOPLEFT', OffsetX, OffsetY * PositionIndex - 17)
	ButtonFrame:Show()
	
	local IconFrame = CreateFrame('Frame', nil, ButtonFrame)
	IconFrame:SetSize(25, 25)
	self.Textures:CheckButtonIcon(IconFrame, self.Rules:GetIcon(Index))
	IconFrame:SetPoint('TOPLEFT', 6, -6)
	IconFrame:Show()
	
	self.Buttons[PositionIndex] = CreateFrame('CheckButton', 'CheckButton' .. PositionIndex, ButtonFrame)
	self.Buttons[PositionIndex]:SetSize(256, 36)
	self.Buttons[PositionIndex]:SetPoint('CENTER')
	self.Textures:CheckButton(self.Buttons[PositionIndex])
	self.Buttons[PositionIndex]:SetScript("OnClick", function(this)
		local HolderFrame = this:GetParent():GetParent()
		local ButtonFrames = {HolderFrame:GetChildren()}
		
		for _, ButtonFrame in ipairs(ButtonFrames) do
			local ButtonObjects = {ButtonFrame:GetChildren()}
			
			for _, ButtonObject in ipairs(ButtonObjects) do
				if ButtonObject:GetObjectType() == 'CheckButton' then
					if ButtonObject:GetName() ~= this:GetName() then
						ButtonObject:SetChecked(0)
					else
						if ButtonObject:GetChecked() then
							_SMARTModeration.InterfaceWorker:SetReasonCode(this:GetName():sub(12) + 1)
						else
							_SMARTModeration.InterfaceWorker:SetReasonCode(0)
						end
					end
				end
			end
		end
	end)
	
	local TitleFS = self.Buttons[PositionIndex]:CreateFontString(nil, 'OVERLAY', 'GameFontNormal')
	TitleFS:SetPoint('TOPLEFT', 42, -5)
	TitleFS:SetText(self.Rules:GetCaption(Index))
	
	self.Captions[PositionIndex] = self.Buttons[PositionIndex]:CreateFontString(nil, 'OVERLAY', 'GameFontNormal')
	self.Captions[PositionIndex]:SetPoint('TOPLEFT', 48, -20)
	self.Captions[PositionIndex]:SetTextColor(1, 1, 1, 0.5)
end