SMARTM_CommandHandler = {}
SMARTM_CommandHandler.__index = SMARTM_CommandHandler

function SMARTM_CommandHandler:Create()
	local CommandHandler = {}
	setmetatable(CommandHandler, SMARTM_CommandHandler)

	--noinspection GlobalCreationOutsideO
	SLASH_SMARTMODERATION1, SLASH_SMARTMODERATION2, SLASH_SMARTMODERATION3 = '/smartmoderation', '/sm', '/smartmod'
	SlashCmdList['SMARTMODERATION'] = CommandHandler.SlashFunction
	
	CommandHandler.hoSetItemRef = SetItemRef
	--noinspection GlobalCreationOutsideO
	SetItemRef = CommandHandler.hfSetItemRef
	
	UnitPopupButtons['SMARTModerationButton'] = {
		text = 'Модерировать',
		dist = 0,
		arg1 = '',
		arg2 = ''
	}	
	tinsert(UnitPopupMenus['PARTY'], #UnitPopupMenus['PARTY'] - 1, 'SMARTModerationButton')
	tinsert(UnitPopupMenus['FRIEND'], #UnitPopupMenus['FRIEND'] - 1, 'SMARTModerationButton')
	tinsert(UnitPopupMenus['SELF'], #UnitPopupMenus['SELF'] - 1, 'SMARTModerationButton')
	tinsert(UnitPopupMenus['PLAYER'], #UnitPopupMenus['PLAYER'] - 1, 'SMARTModerationButton')
	
	UnitPopupButtons['SMARTTeleportButton'] = { 
		text = 'Телепортироваться',
		dist = 0,
		arg1 = '',
		arg2 = ''
	}	
	tinsert(UnitPopupMenus['PARTY'], #UnitPopupMenus['PARTY'] - 1, 'SMARTTeleportButton')
	tinsert(UnitPopupMenus['FRIEND'], #UnitPopupMenus['FRIEND'] - 1, 'SMARTTeleportButton')
	tinsert(UnitPopupMenus['SELF'], #UnitPopupMenus['SELF'] - 1, 'SMARTTeleportButton')
	tinsert(UnitPopupMenus['PLAYER'], #UnitPopupMenus['PLAYER'] - 1, 'SMARTTeleportButton')
	
	hooksecurefunc('UnitPopup_ShowMenu', CommandHandler.MenuFunction)
	
	return CommandHandler
end



function SMARTM_CommandHandler:HookItemRef(link, ...)
	local LinkType = string.sub(link, 1, string.find(link, ':') - 1)
	
	if IsAltKeyDown() then
		if LinkType == 'player' then
			local NameLink = string.sub(link, 8)
			local Name = strsplit(':', NameLink)
			if Name and string.len(Name) > 0 then
				local Begin = string.find(Name, '%s[^%s]+$')
				if Begin then
					Name = string.sub(Name, Begin + 1);
				end
				
				_SMARTModeration.InterfaceWorker:Open(Name)
			end
		end
	else
		self.hoSetItemRef(link, ...)
	end
end


function SMARTM_CommandHandler.SlashFunction(Command)
	_SMARTModeration.InterfaceWorker:Open(Command)
end



function SMARTM_CommandHandler.hfSetItemRef(...)
	_SMARTModeration.CommandHandler:HookItemRef(...)
end



--noinspection UnusedDef
function SMARTM_CommandHandler.MenuFunction(DropdownMenu, Which, Unit, Target)
    if Target then
        _SMARTModeration.InterfaceWorker:SetTarget(Target)
	end

    if Unit then
	    local name = UnitName(Unit)
	    _SMARTModeration.InterfaceWorker:SetTarget(name)
    end
	
	for LoopButtons = 1, UIDROPDOWNMENU_MAXBUTTONS do
		local Button = _G['DropDownList' .. UIDROPDOWNMENU_MENU_LEVEL .. 'Button' .. LoopButtons]
		
		if Button.value == 'SMARTModerationButton' then
			Button.func = _SMARTModeration.InterfaceWorker.OpenWrapper
		end

		if Button.value == 'SMARTTeleportButton' then
			Button.func = (function ()
				_SMARTModeration.CommonWorker:SendCommand('.gon ' .. _SMARTModeration.InterfaceWorker:GetTarget())
			end)
		end
	end
end